import React from 'react'
import { Provider } from 'react-redux'
import { Root } from 'native-base'


import configureStore from './store'

import Navigator from './src/config/routes'

const store = configureStore()

export default () => {

  return (<Provider store={store}>
    <Root>
      <Navigator />
    </Root>
  </Provider>
  )
}
