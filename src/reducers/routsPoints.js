import {POINTS_FETCH, POINTS_FETCH_ERR, POINTS_FETCH_SUCCESS } from '../utils/actionsType'

const initialState = {
    dataPoints: [],
    errPoints: '',
    isFetchingPoints: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case POINTS_FETCH: {
            return {
                ...state,
                isFetchingPoints: true
            }
        }
        case POINTS_FETCH_SUCCESS: {
            return {
                ...state,
                isFetchingPoints: false,
                dataPoints: action.data
            }
        }
        case POINTS_FETCH_ERR: {
            return {
                ...state,
                isFetchingPoints: false,
                errPoints: action.err
            }
        }
        default:
            return state
    }
}