import { SEE_DIRECTION_CONTROLS, SET_BEST_DIRECTIONS, LOADING_DIRECTIONS, ERROR_LOADING_DIRECTIONS, SET_INDEX_TARGET, RESET_DIRECTION } from '../utils/actionsType'

const initialState = {
    seeDirectionControls: false,
    data: [],
    isLoading: false,
    errMsg: '',
    error: false,
    index: 0
}

export default (state = initialState, action) => {
    switch(action.type) {
        case SEE_DIRECTION_CONTROLS: {
            return {
                ...state,
                seeDirectionControls: !state.seeDirectionControls
            }
        }
        case SET_BEST_DIRECTIONS: {
            return {
                ...state,
                data: action.data,
                isLoading: false,
                error: false
            }
        }
        case LOADING_DIRECTIONS: {
            return {
                ...state,
                isLoading: true,
                error: false
            }
        }
        case ERROR_LOADING_DIRECTIONS: {
            return {
                ...state,
                isLoading: false,
                error: true,
                errMsg: action.msg
            }
        }
        case SET_INDEX_TARGET: {
            return {
                ...state,
                index: action.index
            }
        }
        case RESET_DIRECTION: {
            return {
                ...initialState,
                seeDirectionControls: true
            }
        }
        default: {
            return state
        }
    }
}