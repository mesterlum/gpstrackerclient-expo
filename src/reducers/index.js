import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'

import login from './login';
import nav from './nav';
import map from './map'
import menuHome from './menuHome'
import layoutHome from './layoutHome'
import routs from './routs'
import routsPoints from './routsPoints'
import directions from './directions'
import payments from './payments'
import coupons from './coupon'
import user from './user'

export default combineReducers({
    login,
    nav,
    form: formReducer,
    map,
    menuHome,
    layoutHome,
    routs,
    routsPoints,
    directions,
    payments,
    coupons,
    user
})