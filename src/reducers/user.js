import { USER_FETCH, USER_FETCH_ERR, USER_FETCH_SUCCESS } from '../utils/actionsType'

const initialState = {
    loading: false,
    error: false,
    errMsg: '',
    data: {}
}

export default (state = initialState, action) => {
    switch(action.type) {
        case USER_FETCH: {
            return {
                ...state,
                loading: true
            }
        }
        case USER_FETCH_ERR: {
            return {
                ...state,
                loading: false,
                errMsg: action.errMsg,
                error: true
            }
        }
        case USER_FETCH_SUCCESS: {
            return {
                ...state,
                loading: false,
                data: action.data
            }
        }
        default: {
            return state
        }
    }
}