import {
    SET_PERMISSES_CAMERA,
    SET_TYPE_CAMERA,
    SCANNED_QR_CODE,
    RESUME_PAYMENT_FETCH,
    RESUME_PAYMENT_ERR,
    RESUME_PAYMENT_SUCCESS,
    RESET_PAYMENT,
    PAYMENT_FETCH,
    PAYMENT_FETCH_ERR,
    PAYMENT_FETCH_SUCCESS
} from '../utils/actionsType'

import { Camera } from 'expo'

const initialState = {
    scanned: false,
    hasCameraPermission: null,
    typeCamera: Camera.Constants.Type.back,
    vehicle: '',
    errResume: false,
    errResumeMsg: '',
    isLoadingResume: false,
    dataResume: {},
    isLoadingPayment: false,
    errorPayment: false,
    errMsgPayment: '',
    dataPayment: {},
    donePayment: false,
    typeErr: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case PAYMENT_FETCH: {
            return {
                ...state,
                isLoadingPayment: true
            }
        }
        case PAYMENT_FETCH_SUCCESS: {
            return {
                ...state,
                isLoadingPayment: false,
                dataPayment: action.data,
                donePayment: true
            }
        }
        case PAYMENT_FETCH_ERR: {
            return {
                ...state,
                isLoadingPayment: false,
                errorPayment: true,
                errMsgPayment: action.errMsg,
                typeErr: action.typeErr,
                donePayment: true
            }
        }
        case SET_PERMISSES_CAMERA: {
            return {
                ...state,
                hasCameraPermission: action.permise
            }
        }
        case SET_TYPE_CAMERA: {
            return {
                ...state,
                typeCamera: action.typeCamera
            }
        }
        case SCANNED_QR_CODE: {
            return {
                ...state,
                scanned: action.scanned,
                vehicle: action.vehicle
            }
        }
        case RESUME_PAYMENT_FETCH: {
            return {
                ...state,
                isLoadingResume: true
            }
        }
        case RESUME_PAYMENT_ERR: {
            return {
                ...state,
                errResume: true,
                errResumeMsg: action.err,
                isLoadingResume: false
            }
        }
        case RESUME_PAYMENT_SUCCESS: {
            return {
                ...state,
                dataResume: action.data,
                isLoadingResume: false
            }
        }
        case RESET_PAYMENT: {
            return initialState
        }
        default: {
            return state
        }
    }
}