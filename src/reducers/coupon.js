import { CUPON_FETCH, CUPON_FETCH_ERR, CUPON_FETCH_SUCCESS, CUPON_RESET } from '../utils/actionsType'

const initialState = {
    error: false,
    loading: false,
    data: {},
    errMsg: '',
    done: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case CUPON_FETCH: {
            return {
                ...state,
                loading: true
            }
        }
        case CUPON_FETCH_ERR: {
            return {
                ...state,
                loading: false,
                error: true,
                errMsg: action.errMsg
            }
        }
        case CUPON_FETCH_SUCCESS: {
            return {
                ...state,
                loading: false,
                data: action.data,
                done: true
            }
        }
        case CUPON_RESET: {
            return initialState
        } 
        default: {
            return state
        }
    }
}