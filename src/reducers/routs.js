import { ROUTS_FETCH, ROUTS_FETCH_ERR, ROUTS_FETCH_SUCESS, ROUTS_SELECTED_ITEMS } from '../utils/actionsType'

const initialState = {
    isFetching: false,
    error: false,
    data: [],
    dataItems: [],
    errorMessage: '',
    selectedItems: []
}

export default (state = initialState, action) => {

    switch (action.type) {
        case ROUTS_FETCH: {
            return {
                ...state,
                isFetching: true,
                error: false,
                data: [],
                dataItems: [],
                errorMessage: '',
                selectedItems: []
            }
        }
        case ROUTS_FETCH_SUCESS: {
            const items = []
            action.data.data.map(obj => {
                items.push({
                    id: obj._id,
                    name: obj.color
                })
            })

            return {
                ...state,
                isFetching: false,
                error: false,
                dataItems: items,
                data: action.data.data,
                errorMessage: '',
                selectedItems: []
            }
        }
        case ROUTS_FETCH_ERR: {
            return {
                ...state,
                isFetching: false,
                error: true,
                data: [],
                dataItems: [],
                errorMessage: action.errorMessage,
                selectedItems: []
            }
        }
        case ROUTS_SELECTED_ITEMS: {
            return {
                ...state,
                selectedItems: action.selectedItems
            }
        }
        default:
            return state
    }
}