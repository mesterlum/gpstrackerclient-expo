import { CHANGE_STATE_MENU, PICK_ROUTS_VIEW, CHANGE_STATE_FOR_MODAL, CHAGE_STATE_FOR_COUPON_MODAL } from '../utils/actionsType'

const initialState = {
    isOpen: false,
    routsView: false,
    isVisibleModal: false,
    isVisibleModalCoupon: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_STATE_MENU: {
            return {
                ...state,
                isOpen: !state.isOpen
            }
        }
        case PICK_ROUTS_VIEW: {
            return {
                ...state,
                routsView: !state.routsView
            }
        }
        case CHANGE_STATE_FOR_MODAL: {
            return {
                ...state,
                isVisibleModal: !state.isVisibleModal
            }
        }
        case CHAGE_STATE_FOR_COUPON_MODAL: {
            return {
                ...state,
                isVisibleModalCoupon: !state.isVisibleModalCoupon
            }
        }
        default:
            return {
                ...state
            }
    }
}
