import { createStackNavigator, createAppContainer } from 'react-navigation'
import React from 'react'


//Screens
import homeComponent from '../components/layoutHome'
import personalComponent from '../components/homeComponets/personal'
import PaymentsComponent from '../components/payments'
import PaymentsBus from '../components/paymentBus'

import Icon from 'react-native-vector-icons/FontAwesome'


const Navigation = createStackNavigator({
    Home: {
        screen: homeComponent, navigationOptions: {
            header: null
        }
    },
    Personal: {
        screen: personalComponent
    },
    Payments: {
        screen: PaymentsComponent
    },
    PaymentBus: {
        screen: PaymentsBus, navigationOptions: {
            header: null
        }
    }

},
    {
        headerMode: 'float',
    })

export default createAppContainer(Navigation)