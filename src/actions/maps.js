import { MAPS_UPDATE_LOCATION, MAPS_LOAD_POLYGON } from '../utils/actionsType'

function actionMapUdate(coors){
    return {
        type : MAPS_UPDATE_LOCATION,
        coors
    }
}

function actionMapLoadPolygon(dataRout){
    return {
        type: MAPS_LOAD_POLYGON,
        dataRout
    }
}

export function dispatchLoadPolygon(dataRout){
    return dispatch => dispatch(actionMapLoadPolygon(dataRout))
}

export function dispatchGetLocation(geolocation){

    return (dispatch) => {
        geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords
            dispatch(actionMapUdate({latitude, longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421})); // change the latitud...
        }
            , err => console.log(err), {enableHighAccuracy: true, timeout: 10000, maximumAge: 3000})
    }
}