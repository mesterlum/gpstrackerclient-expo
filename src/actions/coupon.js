import { CUPON_FETCH, CUPON_FETCH_ERR, CUPON_FETCH_SUCCESS, CUPON_RESET } from '../utils/actionsType'

import { getToken } from '../utils/utils'
import { URL_API, getHeadersForLogin } from '../../constants'

const actionCuponFetch = () => ({
    type: CUPON_FETCH
})

const actionCuponFetchErr = errMsg => ({
    type: CUPON_FETCH_ERR,
    errMsg
})

const actionCuponFetchSuccess = data => ({
    type: CUPON_FETCH_SUCCESS,
    data
})

const actionCuponReset = () => ({
    type: CUPON_RESET
})

export const dispatchCuponReset = () => dispatch => dispatch(actionCuponReset())

export const dispatchRegisterRedeemCupon = code => async dispatch => {
    dispatch(actionCuponFetch())
    fetch(`${URL_API}/api/cupons/redeem-cupon`, {
        method: 'PUT',
        body: JSON.stringify({ code }),
        headers: getHeadersForLogin(await getToken())
    })
        .then(response => {
            if (response.status == 200)
                response.json().then(data => dispatch(actionCuponFetchSuccess(data)))
            else if (response.status == 404)
                dispatch(actionCuponFetchErr('Cupon no encontrado'))
            else if (response.status == 302)
                dispatch(actionCuponFetchErr('Usted ya registro este cupon'))
        })


        .catch(response => dispatch(actionCuponFetchErr(response)))
}


