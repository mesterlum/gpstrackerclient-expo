import { ROUTS_FETCH, ROUTS_FETCH_SUCESS, ROUTS_FETCH_ERR, ROUTS_SELECTED_ITEMS, POINTS_FETCH, POINTS_FETCH_ERR, POINTS_FETCH_SUCCESS } from '../utils/actionsType'
import { URL_API, getHeadersForLogin } from '../../constants'

function actionRoutsFetch() {
    return {
        type: ROUTS_FETCH
    }
}

function actionSelectedItems(selectedItems) {
    return {
        type: ROUTS_SELECTED_ITEMS,
        selectedItems
    }
}

function actionRoutsFetchSucess(data) {
    return {
        type: ROUTS_FETCH_SUCESS,
        data
    }
}

function actionRoutsFetchErr(errorMessage) {
    return {
        type: ROUTS_FETCH_ERR,
        errorMessage
    }
}

function actionPointsFetch() {
    return {
        type: POINTS_FETCH
    }
}

function actionPointsFetchSuccess(data) {
    return {
        type: POINTS_FETCH_SUCCESS,
        data
    }
}

function actionPointsFetchErr(err) {
    return {
        type: POINTS_FETCH_ERR,
        err
    }
}

export const dispatchSelectedItems = selectedItems => dispatch => dispatch(actionSelectedItems(selectedItems))

export const dispatchRoutsForInitialState = tokenUser => {
    return dispatch => {
        dispatch(actionRoutsFetch())
        fetch(`${URL_API}/api/routes/getrout`, { headers: getHeadersForLogin(tokenUser) })
            .then(json => {
                if (json.status == 200)
                    json.json().then(data => dispatch(actionRoutsFetchSucess(data)))
                else
                    json.json().then(data => dispatch(actionRoutsFetchErr(data.message)))
            })
            .catch(err => dispatch(actionRoutsFetchErr("Intentelo mas tarde")))
    }
}

export const dispatchPoints = tokenUser => {
    return dispatch => {
        dispatch(actionPointsFetch())
        fetch(`${URL_API}/api/routes/get-points`)
            .then(json => {
                if (json.status == 200)
                    json.json().then(data => dispatch(actionPointsFetchSuccess(data)))
                else
                    json.json().then(data => dispatch(actionPointsFetchErr(data.message)))
            })
            .catch(err => dispatch(actionPointsFetchErr("Intentelo mas tarde")))
    }
}