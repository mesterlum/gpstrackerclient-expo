import { CHANGE_STATE_MENU, PICK_ROUTS_VIEW, CHANGE_STATE_FOR_MODAL, CHAGE_STATE_FOR_COUPON_MODAL } from '../utils/actionsType'

function actionChangeStateMenu(){
    return {
        type : CHANGE_STATE_MENU
    }
}

function actionPickRoutsView(){
    return{
        type: PICK_ROUTS_VIEW
    }
}

function actionChangeStateForModal() {
    return {
        type: CHANGE_STATE_FOR_MODAL
    }
}

function actionChangeStateForCouponModal() {
    return {
        type: CHAGE_STATE_FOR_COUPON_MODAL
    }
}

export const dispatchChangeStateMenu = () => dispatch => dispatch(actionChangeStateMenu())
export const dispatchPickRoutsView = () => dispatch => dispatch(actionPickRoutsView())
export const dispatchChangeStateForModal = () => dispatch => dispatch(actionChangeStateForModal())
export const dispatchChangeStateForCouponModal = () => dispatch => dispatch(actionChangeStateForCouponModal())