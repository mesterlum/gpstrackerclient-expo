import {
    SET_PERMISSES_CAMERA,
    SET_TYPE_CAMERA,
    SCANNED_QR_CODE,
    RESUME_PAYMENT_ERR,
    RESUME_PAYMENT_FETCH,
    RESUME_PAYMENT_SUCCESS,
    RESET_PAYMENT,
    PAYMENT_FETCH,
    PAYMENT_FETCH_ERR,
    PAYMENT_FETCH_SUCCESS
} from '../utils/actionsType'

import { getHeadersForLogin, URL_API } from '../../constants'
import { getToken } from '../utils/utils'


const actionSetPermissesCamera = permise => ({
    type: SET_PERMISSES_CAMERA,
    permise
})

const actionSetTypeCamera = typeCamera => ({
    type: SET_TYPE_CAMERA,
    typeCamera
})

const actionScannedQrCode = (scanned, vehicle) => ({
    type: SCANNED_QR_CODE,
    scanned,
    vehicle
})

const actionResumePaymentFetch = () => ({
    type: RESUME_PAYMENT_FETCH
})

const actionResumePaymentSuccess = data => ({
    type: RESUME_PAYMENT_SUCCESS,
    data
})

const actionResumePaymentErr = err => ({
    type: RESUME_PAYMENT_ERR,
    err
})

const actionResetPayment = () => ({
    type: RESET_PAYMENT
})

const actionPaymentFetch = () => ({
    type: PAYMENT_FETCH
})

const actionPaymentFetchErr = (errMsg, typeErr) => ({
    type: PAYMENT_FETCH_ERR,
    errMsg,
    typeErr
})

const actionPaymentFetchSuccess = data => ({
    type: PAYMENT_FETCH_SUCCESS,
    data
})

export const dispatchResetPayment = () => dispatch => dispatch(actionResetPayment())
export const dispatchSetPermissesCamera = permise => dispatch => dispatch(actionSetPermissesCamera(permise))
export const dispatchSetTypeCamera = typeCamera => dispatch => dispatch(actionSetTypeCamera(typeCamera))
export const dispatchScannedQrCode = (scanned, vehicle) => dispatch => dispatch(actionScannedQrCode(scanned, vehicle))

export const dispatchForGetResume = id => async dispatch => {
    let token = await getToken()

    dispatch(actionResumePaymentFetch())
    fetch(`${URL_API}/api/vehicles/get-vehicle/${id}`, { headers: getHeadersForLogin(token) })
        .then(json => {
            if (json.status == 200)
                json.json().then(data => dispatch(actionResumePaymentSuccess(data)))
            else
                json.json().then(data => dispatch(actionResumePaymentErr("Intentalo mas tarde")))
        })
        .catch(err => dispatch(actionResumePaymentErr("Intentelo mas tarde")))

}

export const dispatchMakeAPayment = body => async dispatch => {
    let token = await getToken()
    console.log(body)
    dispatch(actionPaymentFetch())
    fetch(`${URL_API}/api/travels/register-travel`, {
        headers: getHeadersForLogin(token),
        method: 'POST',
        body: JSON.stringify(body)
    })
        .then(response => {
            if (response.status == 200)
                response.json().then(data => dispatch(actionPaymentFetchSuccess(data)))
            else if (response.status == 402)
                dispatch(actionPaymentFetchErr('Saldo insuficiente', 1))
            else if (response.status == 404)
                dispatch(actionPaymentFetchErr('Vehiculo no encontrado', 2))
            else if (response.status == 500)
                dispatch(actionPaymentFetchErr('Error interno', 3))
        })
        .catch(data => dispatch(actionPaymentFetchErr('Error interno', 3)))
}