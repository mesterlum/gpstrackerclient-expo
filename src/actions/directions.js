import { SEE_DIRECTION_CONTROLS, SET_BEST_DIRECTIONS, LOADING_DIRECTIONS, ERROR_LOADING_DIRECTIONS, SET_INDEX_TARGET, RESET_DIRECTION } from '../utils/actionsType'

function actionSeeDirectionControls() {
    return {
        type: SEE_DIRECTION_CONTROLS
    }
}

function actionSetBestDirection(data) {
    return {
        type: SET_BEST_DIRECTIONS,
        data
    }
}

function actionLoadingDirections() {
    return {
        type: LOADING_DIRECTIONS
    }
}

function actionErroLoading(msg) {
    return {
        type: ERROR_LOADING_DIRECTIONS,
        msg
    }
}

function actionSetIndexTarget(index) {
    return {
        type: SET_INDEX_TARGET,
        index
    }
}

function actionResetDirection() {
    return {
        type: RESET_DIRECTION
    }
}

export function dispatchResetDirection() {
    return dispatch => {
        dispatch(actionResetDirection())
    }
}

export function dispatchSeeDirectionControls() {
    return dispatch => {
        dispatch(actionSeeDirectionControls())
    }
}

export function dispatchSetBestDirection(data) {
    return dispatch => {
        dispatch(actionSetBestDirection(data))
    }
}

export function dispatchLoadingDirections() {
    return dispatch => {
        dispatch(actionLoadingDirections())
    }
}

export function dispatchErrLoadingDirections(msg) {
    return dispatch => {
        dispatch(actionErroLoading(msg))
    }
}

export function dispatchSetIndexTarget(index) {
    return dispatch => {
        dispatch(actionSetIndexTarget(index))
    }
}

