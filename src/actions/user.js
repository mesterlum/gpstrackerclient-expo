import {
    USER_FETCH,
    USER_FETCH_ERR,
    USER_FETCH_SUCCESS
} from '../utils/actionsType'

import { getToken } from '../utils/utils'
import { URL_API, getHeadersForLogin } from '../../constants'

const actionUserFetch = () => ({ type: USER_FETCH })
const actionUserSuccess = data => ({ type: USER_FETCH_SUCCESS, data })
const actionUserErr = errMsg => ({type: USER_FETCH_ERR, errMsg})

export const dispatchGetInformation = () => async dispatch => {
    let token = await getToken()
    dispatch(actionUserFetch())

    fetch(`${URL_API}/api/users/get-information`, {
        headers: getHeadersForLogin(token)
    })
    .then(response => {
        if (response.status == 200)
            response.json().then(data => dispatch(actionUserSuccess(data)))
        else if (response.status == 404)
            dispatch(actionUserErr('Usuario no encontrado'))
        else if (response.status == 500)
            dispatch(actionUserErr('Error interno'))
    })
    .catch(_ => dispatch(actionUserErr('Error de conexion')))
}