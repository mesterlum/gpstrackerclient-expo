import React, { Component } from 'react'
import {
    StyleSheet,
    View,
} from 'react-native'
import {
    Container,
    Fab,
    Button,

} from 'native-base'

import { MapView } from 'expo'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'

import Carousel from './map/carouselInMap'


import { dispatchGetLocation, dispatchLoadPolygon } from '../actions/maps'
import { dispatchSeeDirectionControls } from '../actions/directions'
import { dispatchChangeStateForModal } from '../actions/menuHome'

import { dayOrNigth } from '../utils/utils'
import MenuInMap from './map/menuInMap'
import Points from './map/points'
import RoutsPolygon from './map/routsPolygon'
import PaymentBus from './paymentBus'
import Tracking from './map/tracking'

import RoutsComponent from './map/routs'
import RouteDirectionControls from './map/routDirectionToDestin'
import DirectionsInMap from './map/directionsInMap'

import CouponModal from './payments/coupon'


class Map extends Component {



    constructor(props) {
        super(props)
        var propsMap = null
        var mapView = null

    }

    componentDidMount() {
        //Get Hour For MapStyle
        this.propsMap = this.getHourForMapStyle()
        this.props.mapUpdate(navigator.geolocation)
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props == nextProps) {
            return false
        } else {
            return true
        }
    }

    clickToSeeDirections() {
        console.log('aaasda')
        this.setState({ seeDirection: !this.state.seeDirection })
    }

    /*
        This method
    */
    getHourForMapStyle() {
        return dayOrNigth() == 0 ?
            {
                provider: MapView.PROVIDER_GOOGLE,
                customMapStyle: require('../utils/mapStyle').NIGTH
            }
            :
            {
                provider: MapView.PROVIDER_GOOGLE,
                customMapStyle: require('../utils/mapStyle').DAY
            }
    }

    render() {

        const { coors, dataRout } = this.props.mapCors
        return (
            <Container>
                <MapView
                    style={styles.containerMap}
                    showsUserLocation
                    followsUserLocation
                    region={coors}
                    ref={ref => { this.mapView = ref }}
                    {...this.propsMap}
                >

                    {this.props.isViewRouts && <RoutsPolygon dataRout={dataRout} />}

                    {!this.props.seeDirectionControls && <Points />}
                    {this.props.seeDirectionControls && !this.props.isViewRouts && <DirectionsInMap />}
                    <Tracking />
                </MapView>
                <View style={styles.floatControls}>

                    <MenuInMap
                        onPressMenuIcon={this.props.onPressMenuIcon}
                        mapView={this.mapView}
                    />
                    {this.props.seeDirectionControls && <RouteDirectionControls />}
                    <View>
                        {this.props.isViewRouts && <RoutsComponent mapUpdatePolygon={this.props.mapUpdatePolygon} />}
                    </View>
                </View>
                {this.props.seeDirectionControls && !this.props.isViewRouts && <Carousel />}
                <MapWithState isViewRouts={this.props.isViewRouts} seeDirections={this.clickToSeeDirections.bind(this)} />
                <CouponModal isVisible={this.props.isVisibleModalCoupon} clickForClose={this.props.changeStateForCouponModal} />
            </Container>
        )
    }
}

class FabForMap extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            active: 'false'
        };
    }
    render() {
        return (
            <View>
                <PaymentBus isVisible={this.props.isVisibleModal} clickForClose={this.props.onPressForPayments} />
                {
                    !this.props.isViewRouts &&
                    <Fab
                        active={!this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={{ backgroundColor: dayOrNigth() == 0 ? '#AC003F' : '#304E76' }}
                        position="bottomRight"
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon name="bus" color={'white'} />
                        <Button style={{ backgroundColor: '#34A34F' }} onPress={() => {

                            this.props.seeControlsDirections()

                        }}>
                            <Icon name="street-view" color={'white'} size={20} />
                        </Button>
                        <Button style={{ backgroundColor: '#3B5998' }} onPress={() => this.props.onPressForPayments()}>
                            <Icon name="money" color={'white'} size={20} />
                        </Button>

                    </Fab>
                }
            </View>
        )
    }
}

const MapWithState = connect(state => ({
    isVisibleModal: state.menuHome.isVisibleModal
}), dispatch => ({
    seeControlsDirections: () => dispatch(dispatchSeeDirectionControls()),
    onPressForPayments: () => dispatch(dispatchChangeStateForModal())
}))(FabForMap)

const styles = StyleSheet.create({
    containerMap: {
        flex: 1,

    },
    floatControls: {
        position: 'absolute',
        marginTop: 25,
        top: 0,
        left: 0,
        right: 0,
    }
})

function mapStateToProps(state) {

    return {
        mapCors: state.map,
        seeDirectionControls: state.directions.seeDirectionControls
    }
}

function mapDispatchToProps(dispatch) {
    return {
        mapUpdate: (navigator) => dispatch(dispatchGetLocation(navigator)),
        mapUpdatePolygon: data => dispatch(dispatchLoadPolygon(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Map)