import React from 'react'
import { Container, Content, Text, Thumbnail } from 'native-base';
import {
    Image,
    View,
    StyleSheet
} from 'react-native'

import Loading from '../common/loading'

import { connect } from 'react-redux'
import { dispatchGetInformation } from '../../actions/user'

class Personal extends React.Component {
    componentWillMount() {
        this.props.getInformation()
    }
    render() {
        const {
            typeUser,
            money,
            cuponMoney,
            phone, firstName,
            lastName,
            email,
            dateRegister
        } = this.props.user.data
        const {
            errMsg,
            error,
            loading
        } = this.props.user

    
        return (
            <Container style={{ backgroundColor: '#EFEFF4' }}>
                {
                    loading ?
                        <Loading />
                        :
                        error ?
                            <View>
                                <Text>{errMsg}</Text>
                            </View>
                            :
                            <Content>
                                <View style={styles.userCard}>

                                    <Thumbnail square source={require('../../../assets/img/a.jpg')} />
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textWithBorder}>Nombre</Text>
                                        <Text style={styles.textWithBorder}>Nombre</Text>
                                        <Text style={styles.textWithBorder}>Nombre</Text>
                                        <Text style={styles.textWithBorder}>Nombre</Text>
                                    </View>
                                </View>
                            </Content>
                }
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    userCard: {
        marginTop: 8,
        flexDirection: 'row',
        height: 90,
        backgroundColor: 'white'
    },
    textWithBorder: {
        borderBottomWidth: 3,
        borderBottomColor: '#EFEFF4',
    }
})

const mapStateToProps = state => ({
    user: state.user
})

const mapDispatchToProps = dispatch => ({
    getInformation: () => dispatch(dispatchGetInformation())
})

export default connect(mapStateToProps, mapDispatchToProps)(Personal)