import * as React from 'react'

import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions
} from 'react-native'

import {
    Button
} from 'native-base'

const { width, height } = Dimensions.get('window')

import Modal from 'react-native-modal'

type propModal = {
    title: string,
    isVisible: boolean,
    pressForModal: Function
}

export default class extends React.Component<propModal> {

    constructor(props) {
        super(props)
    }


    render() {

        return (
            <Modal
                isVisible={this.props.isVisible}
            >
                <View style={styles.modalContent}>
                    <View style={{
                        borderBottomWidth: 1,
                        borderBottomColor: 'white',
                        marginBottom: 4,
                        paddingBottom: 4
                    }}>
                        <Text>{this.props.title}</Text>
                    <Button full style={styles.button}
                            onPress={() => this.props.pressForModal()}>
                            <Text style={styles.buttonText}>Entiendo</Text>
                        </Button>
                    </View>
                </View>
            </Modal>
        )
    }

}


const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        padding: 22,
        alignSelf: 'center',
        width: width / 1.2,
        height: height / 1.3,
        borderRadius: 10,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    title: {
        fontSize: 17,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    body: {
        height: '90%',
        marginBottom: 5
    },
    button: {
        backgroundColor: 'red'
    },
    buttonText: {
        fontSize: 15,
        color: "white",
        fontWeight: "bold"
    }
})