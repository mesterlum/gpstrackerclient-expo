import * as React from 'react'

import {

} from 'react-native'

import { MapView } from 'expo'

import { connect } from 'react-redux'

import { dispatchPoints } from '../../actions/routs'

import { dayOrNigth } from '../../utils/utils'

class Points extends React.Component {

    componentDidMount() {
        this.props.getPoints()
    }

    render() {
        return (
            <>
                {
                    this.props.points.dataPoints.map((data, index) => (
                        <MapView.Marker key={index} coordinate={{ ...data.geolocation }} title={data.name} pinColor={dayOrNigth() == 0 ? '#AC003F' : '#304E76'}>
                        </MapView.Marker>
                    ))
                }
            </>
        )
    }
}

const mapStateToProps = state => ({
    points: state.routsPoints
})

const mapDispatchToProps = dispatch => ({
    getPoints: () => dispatch(dispatchPoints())
})

export default connect(mapStateToProps, mapDispatchToProps)(Points)