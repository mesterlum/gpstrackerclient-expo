import React from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import { dayOrNigth, getGeolocationAnimated } from '../../utils/utils'

const MenuInMap = props => {

    return (
        <View style={styles.styleMenu} >
            <TouchableOpacity onPress={props.onPressMenuIcon} >
                <Icon
                    name="bars"
                    color={dayOrNigth() == 1 ? 'black' : 'white'}
                    size={35}
                />
            </TouchableOpacity>
            <TouchableOpacity onPress={props.onPressMenuIcon} onPress={() => getGeolocationAnimated(navigator.geolocation, (coors => {
                props.mapView.animateToRegion(coors, 3000)
            }))} >
                <Icon
                    name="location-arrow"

                    color={dayOrNigth() == 1 ? 'black' : 'white'}
                    size={35}
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    styleMenu: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 7
    }
})


export default MenuInMap