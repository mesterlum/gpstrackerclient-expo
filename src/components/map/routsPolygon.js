import * as React from 'react'

import {
    View
} from 'react-native'

import { MapView } from 'expo'

import { connect } from 'react-redux'

import ModalRout from './modalForRouts'



class RoutWithModal extends React.Component {
    state = {
        modal: false
    }

    changeStatusModal = () => this.setState({ modal: !this.state.modal })

    render() {
        return (
            <>
                <ModalRout title={this.props.data.color} isVisble={this.state.modal} pressForModal={this.changeStatusModal} />
                <MapView.Polyline
                    coordinates={this.props.data.pointsGeolocation}
                    strokeColor={this.props.data.colorCode}
                    strokeWidth={4}
                    onPress={() => this.changeStatusModal()}
                />
            </>
        )
    }
}

class RoutsPolygon extends React.Component {

    shouldComponentUpdate(next) {
        if (this.props == next) {
            return false
        } else {
            return true
        }
    }

    render() {
        let data = this.props.dataRout
        if (!Array.isArray(data)) {
            let newArray = []
            newArray.push(data)
            data = newArray
        }

        return (
            <>

                {
                    data.map((obj, index) => (
                        <RoutWithModal data={obj} key={index} />
                    ))
                }
            </>
        )
    }
}


export default connect(null)(RoutsPolygon)