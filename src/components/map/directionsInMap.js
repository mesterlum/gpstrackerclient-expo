import * as React from 'react'
import MapViewDirections from 'react-native-maps-directions'

import {

    StyleSheet,

} from 'react-native'

import { MapView } from 'expo'
import { connect } from 'react-redux'


import { dayOrNigth } from '../../utils/utils'



import { API_KEY_DIRECTIONS } from '../../../constants'




import { dispatchSetIndexTarget } from '../../actions/directions'

import Polygons from './routsPolygon'


const DirectionsInMap = props => {

    const { index } = props.directions


    return (

        <>
            {
                !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                <Polygons dataRout={props.directions.data.routs[index].rout} />
            }

            {
                !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                props.directions.data.routs[index].rout.pointsToCross.map((data, index) => (
                    <MapView.Marker key={index} coordinate={{ ...data.geolocation }} title={data.name} pinColor={dayOrNigth() == 0 ? '#AC003F' : '#304E76'}>
                    </MapView.Marker>
                ))
            }
            {
                !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                <MapViewDirections
                    origin={{ ...props.directions.data.from }}
                    destination={{ ...props.directions.data.routs[index].getOn.geolocation }}
                    apikey={API_KEY_DIRECTIONS}
                    strokeWidth={3}
                    strokeColor={'green'}
                />
            }
            {
                !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                <MapView.Marker coordinate={{ ...props.directions.data.from }} title={'Tu'} pinColor={'green'}></MapView.Marker>
            }
            {
                !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                <MapViewDirections
                    origin={{ ...props.directions.data.getOff.geolocation }}
                    destination={{ ...props.directions.data.to }}
                    apikey={API_KEY_DIRECTIONS}
                    strokeWidth={3}
                    strokeColor={'yellow'}
                />
            }
            {
                !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                <MapView.Marker coordinate={{ ...props.directions.data.to }} title={'Destino'} pinColor={'yellow'}></MapView.Marker>
            }
        </>
    )
}

const styles = StyleSheet.create({
    containerResume: {
        backgroundColor: 'white',
        height: 100,
        borderRadius: 7
    },
    containerCarousel: {
        position: 'absolute',
        width: '100%',
        bottom: 60,
        left: 0,
        right: 0,
    },
    headerCarouselTarget: {
        height: 30,
        padding: 8,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    textInHeader: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold',
        width: '90%'
    }
})

const mapStateToProps = state => ({
    directions: state.directions
})

const mapDispatchToProps = dispatch => ({
    changeIndex: index => dispatch(dispatchSetIndexTarget(index))
})
export default connect(mapStateToProps, mapDispatchToProps)(DirectionsInMap)