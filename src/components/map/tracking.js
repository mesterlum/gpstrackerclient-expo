import * as React from 'react'
import socket from 'socket.io-client'
import {
    View,
    TouchableHighlight,
    Text,
    Platform,
    StyleSheet
} from 'react-native'
import { URL_API } from '../../../constants'

import { MapView } from 'expo'
import { MaterialIcons, FontAwesome, EvilIcons } from '@expo/vector-icons'


import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
    'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?',
    `Can't call setState (or forceUpdate) on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in the componentWillUnmount method.%s`
])


class Tracking extends React.Component {

    state = {
        markers: []
    }

    constructor(props) {
        super(props)
        this.refss = new Array()
    }

    addMarker = (data) => {
        this.setState(state => {
            let coordinate = new MapView.AnimatedRegion({
                latitude: data.gps.latitude,
                longitude: data.gps.longitude
            })
            let markers = state.markers.concat({ data, coordinate })
            return {
                markers
            }
        })
    }

    moveMarker = (marker, index, newCoordinate) => {
        if (Platform.OS === 'android') {
            if (this.refss[index]) {
                this.refss[index]._component.animateMarkerToCoordinate(newCoordinate, 500)
            }
        } else {
            marker.coordinate.timing(newCoordinate).start()
        }
    }

    componentDidMount() {
        this.io = socket.connect(URL_API)

        this.io.on('tracking', data => {

            if (this.state.markers.length == 0) { // Empty array
                this.addMarker(data)
            } else {
                this.state.markers.map((marker, index) => {
                    if (marker.data.gps.imei != data.gps.imei) {
                        this.addMarker(data)
                    } else {
                        let newCoordinate = {
                            latitude: data.gps.latitude,
                            longitude: data.gps.longitude
                        }
                        this.moveMarker(marker, index, newCoordinate)
                    }

                })
            }

        })
    }

    render() {

        //console.log(this.state.markers)
        //console.log(this.state)
        return (
            <>
                {
                    this.state.markers.map((obj, index) => (
                        <MapView.Marker.Animated
                            key={index}
                            ref={marker => {
                                this.refss.push(marker)
                            }}
                            image={require('../../../assets/bus100.png')}
                            coordinate={obj.coordinate}
                            onCalloutPress={() => {
                                this.refss[index].hideCallout();
                                console.log('le preciono')
                            }}
                        >
                            <MapView.Callout
                                tooltip={true}>
                                <View style={styles.calloutContainer}>
                                    <View style={{ ...styles.header, backgroundColor: obj.data.vehicle.rout.colorCode }}>
                                        <Text style={styles.headerTitle}>Ruta: <Text style={{ fontStyle: 'italic' }}>{obj.data.vehicle.rout.color}</Text></Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', height: 25, backgroundColor: '#EFEFF4', alignItems: 'center' }}>
                                        <MaterialIcons name={'attach-money'} size={15} />
                                        <Text>Precio: <Text style={{ fontStyle: 'italic' }}>{obj.data.vehicle.service.price}</Text></Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', height: 25, backgroundColor: 'white', alignItems: 'center' }}>
                                        <FontAwesome name={'stop-circle'} size={15} />
                                        <Text> Paradas: <Text style={{ fontStyle: 'italic' }}>{obj.data.vehicle.rout.pointsToCross.length}</Text></Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', height: 25, backgroundColor: '#EFEFF4', alignItems: 'center' }}>
                                        <EvilIcons name={'refresh'} size={15} />
                                        <Text> Frecuencia: <Text style={{ fontStyle: 'italic' }}>{obj.data.vehicle.rout.frequency}</Text></Text>
                                    </View>
                                </View>
                            </MapView.Callout>
                        </MapView.Marker.Animated>
                    ))
                }
            </>
        )
    }
}

const styles = StyleSheet.create({
    calloutContainer: {
        height: 120,
        width: 120,
        backgroundColor: 'white',
        borderRadius: 5
    },
    header: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold'
    }
})

export default Tracking