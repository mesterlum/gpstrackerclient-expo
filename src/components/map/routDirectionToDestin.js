import * as React from 'react'
import {
    View
} from 'react-native'

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

import { dispatchSetBestDirection, dispatchErrLoadingDirections, dispatchLoadingDirections, dispatchResetDirection } from '../../actions/directions'

import googleMapsClient from 'react-native-google-maps-services'

import { API_KEY_COMPLETE, URL_API, API_KEY_DIRECTIONS } from '../../../constants'

import { connect } from 'react-redux'

class RoutDirectionToDestinControls extends React.Component {

    constructor(props) {
        super(props)

    }

    coors = {}
    items = []

    parseData(coors, dataPoints, isTo = true) {
        let destinations = []

        let dataForApi = []

        dataPoints.map((obj) => {
            dataForApi.push({
                distance: this.distance(obj.geolocation.latitude, obj.geolocation.longitude, this.coors.to.latitude, this.coors.from.longitude, 'K'),
                ...obj
            })
        })
        let routsSort = dataForApi.sort((a, b) => {
            if (a.distance > b.distance) {
                return 1;
            }
            if (a.distance < b.distance) {
                return -1;
            }
            // a must be equal to b
            return 0;
        })

        let newA = []
        let toA = routsSort.length < 10 ? routsSort.length : 10
        for (let i = 0; i < toA; i++) {
            newA.push(routsSort[i])
        }
        dataPoints = newA

        dataPoints.map((obj) => {
            destinations.push(`${obj.geolocation.latitude},${obj.geolocation.longitude}`)
        })
        let origins = []

        for (let i = 0; i < dataPoints.length; i++) {
            if (isTo) {
                origins.push(`${this.coors.to.latitude},${this.coors.to.longitude}`)
            } else {
                origins.push(`${this.coors.from.latitude},${this.coors.from.longitude}`)
            }
        }
        return { destinations, origins, dataPoints }
    }


    async chooseTheWinners(items) {
        const dataToReturn = {}
        const i = await items.sort((a, b) => {
            if (a.distance > b.distance) {
                return 1;
            }
            if (a.distance < b.distance) {
                return -1;
            }
            // a must be equal to b
            return 0;
        })

        dataToReturn.to = this.coors.to
        dataToReturn.from = this.coors.from
        dataToReturn.getOff = i[0].point
        dataToReturn.getOff.duration = i[0].duration / 60
        const data = await fetch(`${URL_API}/api/routes/get-rout-by-point/${i[0].point._id}`)
            .then(data => data.json())
            .then(data => {
                return data
            })
        const bestRout = await data.data.sort((a, b) => {
            if (a.pointsToCross.length > b.pointsToCross.length) {
                return 1;
            }
            if (a.pointsToCross.length < b.pointsToCross.length) {
                return -1;
            }
            // a must be equal to b
            return 0;
        })

        let routsOrder = []

        bestRout.map(async (ro, index) => {
            const routJson = {
                rout: ro
            }
            const { destinations, origins, dataPoints } = this.parseData(this.coors, ro.pointsToCross, false)

            let r = await this.getValuesDistance(destinations, origins)
                .then(async response => {
                    let localData = []
                    response.json.rows[0].elements.map((obj, index) => {
                        localData.push({
                            distance: obj.distance.value,
                            duration: obj.duration.value,
                            point: dataPoints[index]
                        })
                    })
                    let i = await localData.sort((a, b) => {
                        if (a.distance > b.distance) {
                            return 1;
                        }
                        if (a.distance < b.distance) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    })
                    routJson.getOn = i[0].point
                    routJson.getOn.duration = i[0].duration / 60
                    return routJson

                })
                .catch(errMsg => this.props.dispatchErrLoadingDirections(errMsg))
            if (r)
                routsOrder.push(r)
            if (index == bestRout.length - 1) {// for the moment
                dataToReturn.routs = routsOrder
                this.props.dispatchBestDirections(dataToReturn)
            }

        })


    }

    distance(lat1, lon1, lat2, lon2, unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            var radlat1 = Math.PI * lat1 / 180;
            var radlat2 = Math.PI * lat2 / 180;
            var theta = lon1 - lon2;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit == "K") { dist = dist * 1.609344 }
            if (unit == "N") { dist = dist * 0.8684 }
            return dist;
        }
    }


    getValuesDistance(destinations, origins) {
        let m = googleMapsClient.createClient({
            key: API_KEY_DIRECTIONS
        })
        console.log(destinations, origins)
        return new Promise((resolve, reject) => {
            m.distanceMatrix({
                origins,
                destinations
            }, function (err, response) {

                if (!err) {
                    // Handle response.
                    resolve(response)
                } else if (err === 'timeout') {
                    reject('Se agoto el tiempo de carga...')
                    // Handle timeout.
                } else if (err.json) {
                    // Inspect err.status for more info.
                    console.log(err.json)
                    reject('Error al intentar obtener los datos...')
                } else {
                    // Handle network error.
                    reject('No hay conexion')
                }

            })

        })

    }

    getDistance(coors) {
        var self = this

        const { destinations, origins, dataPoints } = this.parseData(coors, this.props.points.dataPoints)

        this.getValuesDistance(destinations, origins)
            .then(response => {
                response.json.rows[0].elements.map((obj, index) => {
                    self.items.push({
                        distance: obj.distance.value,
                        duration: obj.duration.value,
                        point: dataPoints[index]
                    })
                })
                self.chooseTheWinners(self.items)
            })
            .catch(errMsg => this.props.dispatchErrLoadingDirections(errMsg))
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords
            this.coors.from = { latitude, longitude }
        }
            , err => console.log(err), { enableHighAccuracy: true, timeout: 10000, maximumAge: 3000 })
    }

    checkIfCoorsHaveInformation() {
        if (this.coors.from && this.coors.to) {
            this.props.resetDirection()
            this.props.dispatchLoadingDirections()
            this.getDistance(this.coors)
        }

    }

    render() {
        //this.getDistance(this.coors)
        return (
            <View>
                <GooglePlacesAutocomplete
                    placeholder='Desde donde (defecto tu location)'
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'default'}
                    fetchDetails={true}
                    onPress={(_, details = null) => { // 'details' is provided when fetchDetails = true
                        this.coors.from = { latitude: details.geometry.location.lat, longitude: details.geometry.location.lng }
                        this.checkIfCoorsHaveInformation()
                    }}
                    styles={{
                        textInputContainer: {
                            backgroundColor: 'rgba(0,0,0,0)',
                            borderTopWidth: 0,
                            borderBottomWidth: 0,
                            width: '85%',
                            alignSelf: 'center',
                            marginBottom: 5
                        },
                        textInput: {
                            marginLeft: 0,
                            marginRight: 0,
                            height: 38,
                            color: '#5d5d5d',
                            fontSize: 16,
                        },
                        predefinedPlacesDescription: {
                            color: '#1faadb',
                        },
                        listView: {
                            backgroundColor: 'white',
                            width: '85%',
                            alignSelf: 'center'
                        }
                    }}
                    currentLocation={false}
                    query={{
                        // available options: https://developers.google.com/places/web-service/autocomplete
                        key: API_KEY_COMPLETE,
                    }}
                />
                <GooglePlacesAutocomplete
                    placeholder='A donde'
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'default'}
                    fetchDetails={true}
                    onPress={(_, details = null) => { // 'details' is provided when fetchDetails = true
                        this.coors.to = { latitude: details.geometry.location.lat, longitude: details.geometry.location.lng }
                        this.checkIfCoorsHaveInformation()
                    }}
                    styles={{
                        textInputContainer: {
                            backgroundColor: 'rgba(0,0,0,0)',
                            borderTopWidth: 0,
                            borderBottomWidth: 0,
                            width: '85%',
                            alignSelf: 'center'
                        },
                        textInput: {
                            marginLeft: 0,
                            marginRight: 0,
                            height: 38,
                            color: '#5d5d5d',
                            fontSize: 16
                        },
                        predefinedPlacesDescription: {
                            color: '#1faadb'
                        },
                        listView: {
                            backgroundColor: 'white',
                            width: '85%',
                            alignSelf: 'center'
                        }
                    }}
                    currentLocation={false}
                    query={{
                        // available options: https://developers.google.com/places/web-service/autocomplete
                        key: API_KEY_COMPLETE,
                    }}
                />
            </View>
        )
    }
}

const mapStateToProps = state => ({
    points: state.routsPoints
})

const mapDispatchToProps = dispatch => ({
    dispatchBestDirections: data => dispatch(dispatchSetBestDirection(data)),
    dispatchErrLoadingDirections: msg => dispatch(dispatchErrLoadingDirections(msg)),
    dispatchLoadingDirections: () => dispatch(dispatchLoadingDirections()),
    resetDirection: () => dispatch(dispatchResetDirection())

})

export default connect(mapStateToProps, mapDispatchToProps)(RoutDirectionToDestinControls)