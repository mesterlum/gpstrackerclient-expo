import * as React from 'react'


import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native'


import { connect } from 'react-redux'
import Carousel from 'react-native-snap-carousel'



import { FontAwesome } from '@expo/vector-icons'



const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window')


function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const slideWidth = wp(75)
const itemHorizontalMargin = wp(2)
const slideHeight = viewportHeight * 0.36
const sliderWidth = viewportWidth;

const itemWidth = slideWidth + itemHorizontalMargin * 2

import { dispatchSetIndexTarget } from '../../actions/directions'

import Polygons from './routsPolygon'


const CarouselInMAP = props => {

    const { index } = props.directions

    const Target = ({ item, index }) => {

        return (
            <View style={styles.containerResume}>
                <View style={{ ...styles.headerCarouselTarget, backgroundColor: item.rout.colorCode }}>
                    <Text style={styles.textInHeader}>Linea: <Text style={{ fontSize: 11, fontWeight: '800' }}>{item.rout.color}</Text></Text>
                    {index == 0 && <FontAwesome name={'star'} size={14} color={'white'} />}
                    {index == 1 && <FontAwesome name={'star-o'} size={14} color={'white'} />}
                </View>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ marginLeft: 8 }}>
                        <Image source={require('../../../assets/bus2.png')} />
                        <Text>Frecuencia</Text>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View>

            <View style={styles.containerCarousel}>
                {
                    !props.directions.error && !props.directions.isLoading && props.directions.data && props.directions.data.routs &&
                    <Carousel layout={'default'}
                        data={props.directions.data.routs}
                        renderItem={Target}
                        itemWidth={itemWidth}
                        sliderWidth={sliderWidth}
                        windowSize={1}
                        onSnapToItem={(index) => props.changeIndex(index)}
                    />
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerResume: {
        backgroundColor: 'white',
        height: 100,
        borderRadius: 7
    },
    containerCarousel: {
        position: 'absolute',
        width: '100%',
        bottom: 60,
        left: 0,
        right: 0,
    },
    headerCarouselTarget: {
        height: 40,
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    textInHeader: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold',
        width: '90%'
    }
})

const mapStateToProps = state => ({
    directions: state.directions
})

const mapDispatchToProps = dispatch => ({
    changeIndex: index => dispatch(dispatchSetIndexTarget(index))
})
export default connect(mapStateToProps, mapDispatchToProps)(CarouselInMAP)