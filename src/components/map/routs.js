import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  AsyncStorage,
  ActivityIndicator
} from 'react-native'
import MultiSelect from 'react-native-multiple-select';
import { connect } from 'react-redux'

import { dispatchRoutsForInitialState, dispatchSelectedItems } from '../../actions/routs'

class Routs extends Component {

  constructor(props) {
    super(props)

  }

  componentDidMount() {
    AsyncStorage.getItem('tokenUser', (err, userToken) => {
      this.props.getRouts(userToken)
    })
  }

  getDataFilterPolygon(data, selectedItems) {
    const items = []
    data.map(obj => {
      selectedItems.map(obj2 => {
        if (obj._id === obj2)
          items.push(obj)
      })
    })
    return items
  }

  shouldComponentUpdate(nextProps) {
    if (this.props == nextProps){
      return false
    } else {
      return true
    }
  }

  render() {
    const { data, dataItems, error, errorMessage, isfetching, selectedItems } = this.props.routsState
    if (selectedItems.length > 0)
      this.props.mapUpdatePolygon(this.getDataFilterPolygon(data, selectedItems))
    else
      if (data.length > 0 && error == false)
        this.props.mapUpdatePolygon(data)
    if (!dataItems)
        dataItems = [
          {
            id: 1,
            item: 'getting data...'
          }
        ]
      return (
        <View style={styles.container}>
          {
            isfetching ? <ActivityIndicator style={{ alignSelf: 'center' }} size="large" color="#00ff00" />
              : error ? <Text style={{ alignSelf: 'center' }}> {errorMessage} </Text>
                : <MultiSelect
                  hideTags
                  items={dataItems}
                  uniqueKey="id"
                  onSelectedItemsChange={this.props.onSelectedItems}
                  selectedItems={selectedItems}
                  selectText="Seleccione las rutas"
                  searchInputPlaceholderText="Buscar rutas..."
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="name"
                  searchInputStyle={{ color: '#CCC' }}
                  submitButtonColor="#CCC"
                  submitButtonText="Submit"
                />
          }

        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginTop: 10
  }
})

function mapStateToProps(state) {
  return {
    routsState: state.routs
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getRouts: tokenUser => dispatch(dispatchRoutsForInitialState(tokenUser)),
    onSelectedItems: items => dispatch(dispatchSelectedItems(items))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Routs)