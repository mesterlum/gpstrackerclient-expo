import React from 'react'
import { Text, View, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'

import { connect } from 'react-redux'

import { AntDesign } from '@expo/vector-icons'

const { width, height } = Dimensions.get('window')

import Modal from 'react-native-modal'
import ScanQr from './payments/scanQr'
import Summary from './payments/summary'

import {
  dispatchScannedQrCode,
  dispatchSetPermissesCamera,
  dispatchSetTypeCamera,
  dispatchForGetResume,
  dispatchResetPayment,
  dispatchMakeAPayment
} from '../actions/payments'


type Props = {
  isVisible: boolean,
  clickForClose: Function
}

const PaymentsBus = props => {
  const {
    hasCameraPermission,
    scanned,
    typeCamera,
    vehicle,
    dataResume,
    errResume,
    isLoadingResume,
    errResumeMsg,
    isLoadingPayment,
    errorPayment,
    errMsgPayment,
    dataPayment,
    donePayment,
    typeErr
  } = props.payments
  return (
    <Modal
      isVisible={props.isVisible}
    >
      <View style={styles.ForClose}>
        <TouchableOpacity style={styles.buttonClose} onPress={() => {
          props.resetPayment()
          props.clickForClose()
        }}>
          <AntDesign name="closecircle" size={30} color="white" />
        </TouchableOpacity>
      </View>
      <View style={{ ...styles.modalContent, height: scanned ? 350 : height / 1.3}}>
        {
          !scanned ? <ScanQr hasCameraPermission={hasCameraPermission}
            scanned={scanned}
            typeCamere={typeCamera}
            scannedQrCode={props.scannedQrCode}
            setPermissesCamera={props.setPermissesCamera}
            setTypeCamera={props.setTypeCamera}
          />
            : <Summary id={vehicle}
              getResume={props.getResume}
              data={dataResume}
              err={errResume}
              loading={isLoadingResume}
              errMsg={errResumeMsg}
              resetPayment={props.resetPayment}
              makeAPayment={props.makeAPayment}
              isLoadingPayment={isLoadingPayment}
              errorPayment={errorPayment}
              errMsgPayment={errMsgPayment}
              dataPayment={dataPayment}
              donePayment={donePayment}
              typeErr={typeErr}
            />

        }

      </View>
    </Modal >
  )
}

const mapStateToProps = state => ({
  payments: state.payments
})

const mapDispatchToProps = dispatch => ({
  scannedQrCode: (scanned, vehicle) => dispatch(dispatchScannedQrCode(scanned, vehicle)),
  setPermissesCamera: permise => dispatch(dispatchSetPermissesCamera(permise)),
  setTypeCamera: typeCamera => dispatch(dispatchSetTypeCamera(typeCamera)),
  getResume: id => dispatch(dispatchForGetResume(id)),
  resetPayment: () => dispatch(dispatchResetPayment()),
  makeAPayment: body => dispatch(dispatchMakeAPayment(body))
})

export default connect(mapStateToProps, mapDispatchToProps)(PaymentsBus)



const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: "white",
    alignSelf: 'center',
    width: width / 1.2,
    borderColor: "rgba(0, 0, 0, 0.1)",
    borderRadius: 5,
    padding: 5
  },
  bottonForCamera: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30
  },
  ForClose: {
    flexDirection: 'row-reverse'
  },
  buttonClose: {
    width: 40,
    marginBottom: 5
  }
})