import React from 'react'
import {
    Text, View, TouchableOpacity, Dimensions, StyleSheet, KeyboardAvoidingView,
    Image
} from 'react-native'

import { connect } from 'react-redux'

import { AntDesign } from '@expo/vector-icons'

const { width } = Dimensions.get('window')

import Modal from 'react-native-modal'
import {
    Icon,
    Input,
    Item,
    Button
} from 'native-base'

import { BarIndicator } from 'react-native-indicators'

const Loading = () => (
    <View style={styles.loadingIndication}>
        <BarIndicator size={25} color={'#D3960B'} count={7} />
        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Cargando...</Text>
    </View>
)

import { dispatchRegisterRedeemCupon, dispatchCuponReset } from '../../actions/coupon'

type Props = {
    isVisible: boolean,
    clickForClose: Function
}
const Coupon = (props: Props) => {
    const {
        loading,
        data: { amount },
        error,
        errMsg,
        done
    } = props.coupon

    return (
        <Modal
            isVisible={props.isVisible}
        >
            <KeyboardAvoidingView behavior="padding" style={{
                flex: 1,
                justifyContent: 'center'
            }} enabled>
                <View style={styles.ForClose}>
                    <TouchableOpacity style={styles.buttonClose} onPress={() => {
                        props.resetCoupon()
                        props.clickForClose()
                    }}>
                        <AntDesign name="closecircle" size={30} color="white" />
                    </TouchableOpacity>
                </View>
                <View style={{ ...styles.modalContent, height: 180 }} behavior="padding" enabled>

                    <View style={styles.header}>
                        <Text style={styles.textHeader}>CANJEAR UN CUPON</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        marginVertical: 5
                    }}>
                        {
                            !loading && !done && !error &&
                            <Controls registerCoupon={props.registerCoupon} />
                        }
                        {
                            loading && <Loading />
                        }
                        {
                            error && <View style={styles.wrapperResponse}>
                                <Image source={require('../../../assets/error.png')} style={{ width: 70, height: 70 }} />
                                <Text style={{ fontSize: 17 }}>{errMsg}</Text>
                                <Button style={{
                                    backgroundColor: '#936817',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: '50%',
                                    height: 25,
                                    borderRadius: 4,
                                    marginTop: 10,
                                    alignSelf: 'center'
                                }}
                                    onPress={props.resetCoupon}
                                >
                                    <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white' }}>Re intentar</Text>
                                </Button>
                            </View>
                        }
                        {
                            !error && done &&
                            <View style={styles.wrapperResponse}>
                                <Image source={require('../../../assets/correct.png')} style={{ width: 70, height: 70 }} />
                                <Text style={{ fontSize: 17, textAlign: 'center' }}>Cupon canjeado con un valor de <Text style={{ fontWeight: 'bold', fontStyle: 'italic' }}>{amount}</Text> pesos!</Text>

                            </View>
                        }
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Modal >
    )
}

class Controls extends React.Component {
    state = {
        field: ''
    }


    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                marginHorizontal: 10
            }}>
                <Item>
                    <Icon active name='ticket' type={'FontAwesome'} />
                    <Input
                        placeholder='INTRODUCE CODIGO'
                        style={{ textAlign: 'center' }}
                        onChangeText={(text) => this.setState({ field: text.toUpperCase() })}
                        value={this.state.field}
                    />
                </Item>
                <Button full style={{ backgroundColor: 'black' }} onPress={() => this.props.registerCoupon(this.state.field)}>
                    <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>CANJEAR</Text>
                </Button>
            </View >
        )
    }
}

const mapStateToProps = state => ({
    coupon: state.coupons
})

const mapDispatchToProps = dispatch => ({
    registerCoupon: code => dispatch(dispatchRegisterRedeemCupon(code)),
    resetCoupon: () => dispatch(dispatchCuponReset())
})

export default connect(mapStateToProps, mapDispatchToProps)(Coupon)



const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: "white",
        alignSelf: 'center',
        width: width / 1.2,
        borderColor: "rgba(0, 0, 0, 0.1)",
        borderRadius: 5,
    },
    bottonForCamera: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 30
    },
    ForClose: {
        flexDirection: 'row-reverse'
    },
    buttonClose: {
        width: 40,
        marginBottom: 5
    },
    header: {
        height: 40,
        backgroundColor: '#463C37',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textHeader: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    },
    loadingIndication: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    wrapperResponse: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }
})