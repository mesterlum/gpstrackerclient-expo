import * as React from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity as Button,
    Image
} from 'react-native'
import {
    ActionSheet
} from 'native-base'


import { BarIndicator } from 'react-native-indicators'

import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons'

const Loading = () => (
    <View style={styles.loadingIndication}>
        <BarIndicator size={25} color={'#D3960B'} count={7} />
        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Cargando...</Text>
    </View>
)

class Summary extends React.Component {


    componentDidMount() {
        this.props.getResume(this.props.id)
    }

    getYearsWorked = timeStamp => {
        let date1 = new Date(timeStamp * 1000)
        let date2 = new Date()
        let timeDiff = Math.abs(date2.getTime() - date1.getTime())
        let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
        let yearsExperience = (diffDays / 365) | 0
        return yearsExperience > 0 ? yearsExperience : 1
    }

    selectPaymentMethod = () => {

        const BUTTONS = ["Dinero electronico", "Tarjeta terminada en *554", "Delete", "Cancel"]
        const DESTRUCTIVE_INDEX = 2
        const CANCEL_INDEX = 3
        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                destructiveButtonIndex: DESTRUCTIVE_INDEX,
                title: "Selecciona metodo de pago..."
            },
            buttonIndex => {
                console.log(buttonIndex)
            }
        )

    }

    makeAPayment = () => {
        const {
            data: { vehicle },
        } = this.props
        this.props.makeAPayment({
            vehicle: vehicle._id,
            typePayment: 1
        })
    }


    render() {
        //console.log(this.props)
        const {
            data: { vehicle },
            loading,
            err,
            errMsg,
            isLoadingPayment,
            errorPayment,
            errMsgPayment,
            dataPayment,
            donePayment,
            typeErr
        } = this.props

        let yearsWorked = 0
        if (vehicle) {
            yearsWorked = this.getYearsWorked(vehicle.employeeOperator.dateRegister)
        }
        return (
            <View style={{ flex: 1 }}>
                {
                    loading ?
                        <Loading />
                        :
                        vehicle ?
                            <View style={{ marginHorizontal: 5, flex: 1 }}>
                                <View style={styles.header}>
                                    <Text style={styles.headerText}>INFORMACION</Text>
                                </View>
                                <View style={styles.employeeResume}>
                                    <Image source={require('../../../assets/resume/licence.png')} />
                                    <View style={styles.employeResumeInformation}>
                                        <Text
                                            numberOfLines={1}
                                            ellipsizeMode='tail'
                                            style={styles.textForWrap}
                                        >Nombre: <Text style={styles.textWithItalic}>{vehicle.employeeOperator.firstName} {vehicle.employeeOperator.lastName}</Text></Text>
                                        <Text
                                            numberOfLines={1}
                                            ellipsizeMode='tail'
                                            style={styles.textForWrap}
                                        >Experiencia: <Text style={styles.textWithItalic}>{yearsWorked} año {yearsWorked > 1 && 's'}</Text></Text>

                                    </View>
                                </View>
                                <View style={styles.busResume}>
                                    <Image source={require('../../../assets/resume/bus.png')} />
                                    <View style={styles.busResumeInformation}>
                                        <Text
                                            numberOfLines={1}
                                            ellipsizeMode='tail'
                                            style={styles.textForWrap}
                                        >Matricula: <Text style={styles.textWithItalic}>{vehicle.matricule}</Text></Text>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center'
                                        }}>
                                            <Text
                                                numberOfLines={1}
                                                ellipsizeMode='tail'
                                                style={styles.textForWrap}
                                            >Ruta: </Text>
                                            <MaterialCommunityIcons name={'solid'} style={{ marginHorizontal: 5 }} size={10} color={vehicle.rout.colorCode} />
                                            <Text style={styles.textWithItalic}>{vehicle.rout.color}</Text>
                                        </View>
                                        <Text
                                            numberOfLines={1}
                                            ellipsizeMode='tail'
                                            style={styles.textForWrap}
                                        ># Paradas: <Text style={styles.textWithItalic}>{vehicle.rout.pointsToCross.length}</Text></Text>
                                        <Text
                                            numberOfLines={1}
                                            ellipsizeMode='tail'
                                            style={styles.textForWrap}
                                        >Precio: <Text style={styles.textWithItalic}>${vehicle.service.price}</Text></Text>
                                    </View>

                                </View>
                                <View style={styles.payment}>
                                    {
                                        isLoadingPayment ?
                                            <Loading />
                                            :
                                            !donePayment ?
                                                <View style={styles.wrapperControls}>
                                                    <Button style={{
                                                        ...styles.buttonPayment,
                                                        width: '80%',
                                                        marginRight: 3,
                                                    }}
                                                        onPress={this.makeAPayment}
                                                    ><Text style={styles.textPayment}>PAGAR</Text></Button>
                                                    <Button style={{
                                                        ...styles.buttonPayment,
                                                        width: '20%',
                                                        borderLeftWidth: 1,
                                                        borderLeftColor: 'white'
                                                    }}
                                                        onPress={this.selectPaymentMethod}
                                                    >
                                                        <MaterialIcons name={'payment'} color={'white'} size={20} />
                                                    </Button>
                                                </View>
                                                :
                                                errorPayment ?
                                                    <View style={{
                                                        ...styles.flexBox, backgroundColor: '#9E0E1D', flex: 1,
                                                        marginTop: 5
                                                    }}>
                                                        <Image source={require('../../../assets/error.png')} style={{ height: 50, width: 50 }} />
                                                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold', marginBottom: 5 }}>{errMsgPayment}</Text>
                                                        {
                                                            typeErr != 1 &&
                                                            <Button style={{
                                                                backgroundColor: 'white',
                                                                height: 30,
                                                                padding: 5,
                                                                justifyContent: 'center',
                                                                alignItems: 'center'
                                                            }}><Text style={{ fontSize: 13 }}>Reintentar</Text></Button>
                                                        }
                                                    </View>
                                                    :
                                                    <View style={{
                                                        ...styles.flexBox, backgroundColor: '#106D2C', flex: 1,
                                                        marginTop: 5
                                                    }}>
                                                        <Image source={require('../../../assets/correct.png')} style={{ height: 50, width: 50 }} />
                                                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
                                                            Pago realizado por <Text style={{fontStyle: 'italic'}}>${dataPayment.amountPaid}</Text>
                                                        </Text>
                                                    </View>


                                    }
                                </View>
                            </View> :
                            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                <Image source={require('../../../assets/error.png')} />
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>No se encontro este camion!</Text>
                                <Button style={{
                                    backgroundColor: '#304E76',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: '50%',
                                    height: 25,
                                    borderRadius: 4,
                                    marginTop: 10
                                }}
                                    onPress={this.props.resetPayment}
                                >
                                    <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white' }}>Re intentar</Text>
                                </Button>
                            </View>

                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#304E76'
    },
    headerText: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    },
    employeeResume: {
        height: 80,
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 5
    },
    employeResumeInformation: {
        justifyContent: 'center',
        flex: 1
    },
    busResume: {
        height: 80,
        alignItems: 'center',
        flexDirection: 'row-reverse'
    },
    busResumeInformation: {
        justifyContent: 'center',
        flex: 1,
        marginLeft: 20
    },
    loadingIndication: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    textForWrap: {
        fontSize: 13,
        fontWeight: '500'
    },
    textWithItalic: {
        fontStyle: 'italic',
        fontWeight: '400'
    },
    payment: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    buttonPayment: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 29,
    },
    textPayment: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 12
    },
    wrapperControls: {
        flexDirection: 'row',
        backgroundColor: '#270404',
        width: '80%',
        borderRadius: 5
    },
    flexBox: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    }
})

export default Summary