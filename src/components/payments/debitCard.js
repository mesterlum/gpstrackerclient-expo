import React from 'react'

import {
    Container,
    Text,
    Content,
    Form,
    Item,
    Label,
    Input,
    Icon,
    Picker,
    Button
} from 'native-base'

import {
    View,
    Platform,
    StyleSheet
} from 'react-native'

import { CreditCardInput } from "react-native-credit-card-input";

class DebitCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selected1: "visa"
        }

    }

    _onChange = (formData) => console.log(JSON.stringify(formData, null, " "));
    _onFocus = (field) => console.log("focusing", field);

    render() {
        return (
            <Container>
                <Content>
                    <CreditCardInput
                        autoFocus
                        requiresCVC

                        labelStyle={s.label}
                        inputStyle={s.input}
                        validColor={"black"}
                        invalidColor={"red"}
                        placeholderColor={"darkgray"}

                        onFocus={this._onFocus}
                        onChange={this._onChange} />
                </Content>
            </Container>
        )
    }
}

const s = StyleSheet.create({
    container: {
        backgroundColor: "#F5F5F5",
        marginTop: 60,
    },
    label: {
        color: "black",
        fontSize: 12,
    },
    input: {
        fontSize: 16,
        color: "black",
    },
});


export default DebitCard