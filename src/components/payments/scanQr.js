import React from 'react';
import {
    Text, View,
    StyleSheet
} from 'react-native'
import {
    Button
} from 'native-base'
import { Camera, Permissions, BarCodeScanner, Constants } from 'expo'

import { DotIndicator } from 'react-native-indicators'
import DialogInput from 'react-native-dialog-input'

export default class CameraExample extends React.Component {
    state = {
        isDialogVisible: false
    }
    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.props.setPermissesCamera(status === 'granted')
    }

    handleBarCodeScanned = ({ data }) => {
        this.props.scannedQrCode(true, data)
    }

    showHelp = b => this.setState({isDialogVisible: b})

    render() {
        const { hasCameraPermission, scanned } = this.props;
        if (hasCameraPermission === null) {
            return <View />
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <View style={styles.header}><Text style={styles.textHeader}>Por favor escanea el codigo QR.</Text><DotIndicator size={4} color={'black'} /></View>
                    {
                        Constants.isDevice &&
                        <Camera style={{ flex: 1 }}
                            type={this.props.typeCamera}
                            barCodeScannerSettings={{
                                barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr]
                            }}
                            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                        >
                        </Camera>
                    }
                    <Button style={styles.buttonProblem} onPress={() => this.showHelp(true)}>
                        <Text style={styles.textInButton}>TENGO UN PROBLEMA</Text>
                    </Button>
                    <DialogInput isDialogVisible={this.state.isDialogVisible}
                        title={"Tienes un problema?"}
                        message={"Introduce la matricula del camion"}
                        hintInput={"Matricula"}
                        submitInput={(inputText) => {
                            this.props.scannedQrCode(true, inputText)
                            this.showHelp(false)
                        }}
                        closeDialog={() => { this.showHelp(false) }}>
                    </DialogInput>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    header: {
        height: 40,
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    textHeader: {
        fontSize: 14,
        fontWeight: '500'
    },
    buttonProblem: {
        backgroundColor: '#AC003F',
        marginVertical: 10,
        width: '50%',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    textInButton: {
        fontSize: 12,
        color: 'white',
        fontWeight: 'bold',
    }
})