import React from 'react'

import {
    Container,
    Header,
    Body,
    Left,
    Icon,
    Button,
    Title,
    Right,
    Tabs,
    Tab,
    Text,
    TabHeading,
    Root
} from 'native-base'

import { Font, AppLoading } from "expo";

import Icon2 from 'react-native-vector-icons/FontAwesome'

import DebitCard from './debitCard'

class Payments extends React.Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
    }

    async componentWillMount() {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
        });
        this.setState({ loading: false });
    }

    render() {
        if (this.state.loading) {
            return (
                <Root>
                    <AppLoading />
                </Root>
            )
        }
        return (
            <Container>
                <DebitCard />
            </Container>

        )
    }
}





export default Payments