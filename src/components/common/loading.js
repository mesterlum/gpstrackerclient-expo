import * as React from 'react'

import {
    View,
    Text
} from 'react-native'

import { BarIndicator } from 'react-native-indicators'

const Loading = () => (
    <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }}>
        <BarIndicator size={25} color={'#D3960B'} count={7} />
        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Cargando...</Text>
    </View >
)

export default Loading